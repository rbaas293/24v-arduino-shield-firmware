/*
 By: Ryan Baas
 Date: 02-02-18

 This is a Proof of Concept (POC) for the Protected Switch Shield with PROFET™+ 24V for Arduino
 A link to the sheild is here: https://www.mouser.com/ProductDetail/Infineon-Technologies/24VSHIELDBTT6030TOBO1?qs=%2fha2pyFaduibcMzkjvBjZOS5%252b0ffteNrOTUiDPnmPAnzNg1OKyGkCeC%2flxO1gf6w

 The sheild includes 3x PROFET +24V: which are comprised by: two BTT6030-2EKA and one BTT6020-1EKA PROFET.
 Each is built by a vertical N-channel power MOSFET with charge pump. The Charge pump enables the channels to be controled by
 standard 5 or 3v TTL digital logic.

02-07-18: VERSION 1.0


*/
#include <SerialCommand.h>
//#include <SoftwareSerial.h>
//#include <EEPROM.h>
#define kSerialBaudRate 9600
/*
 ALL BELOW ARE INPUTS TO SHEILD. THE SHEILD TAKES THEM AND GIVES US THE CORRISPONDING 24V OUTPUT. SO WE CAN JUST CALL THEM OUTPUTS.
 NAME FORMAT: OUT1_0 IS OUTPUT 2 of the first PROFET CHIP.
*/
#define OUT0_0 A3     //Sheild Output 1
#define OUT1_0 3      //Sheild Output 2
#define OUT0_1 4      //Sheild Output 3
#define OUT1_1 7      //Sheild Output 4
#define OUT0_2 8      //Sheild Output 5
/*
 Defined below are the easy to read identifiers. can use either one
*/
#define O1 A3      //Sheild Output 1
#define O2 3       //Sheild Output 2
#define O3 4       //Sheild Output 3
#define O4 7       //Sheild Output 4
#define O5 8       //Sheild Output 5

//Set Up Objects:
SerialCommand gSerialCommands;

//Set Up Functions/Methods:
char GetCurState(int iOutNum);    //gets the CURRENT state of the specified pin number.
bool GetNextState(char *iOutNum); //gets the NEXT state of the specified pin number.
char *i2str(int i, char *buf);    //int to string fuction, cuz sting(int) would not work.

//Set Up Variables and Arrays
unsigned long gScanRate;
unsigned long gScanRateMin;
unsigned long gScanRateMax;
unsigned long ScanStartMicros;
char gAck[4];
char gNack[5];
char gbuf[8];
char* gOutAliases[]={"O1", "O2", "O3", "O4", "O5"};


void setup() {
// put your setup code here, to run once:
 //cli();//stop interrupts
 SetupSerialCommands();
 gScanRateMin = 999999;
 gScanRateMax = 0;
 strcpy(gAck, "ACK");
 strcpy(gNack, "NACK");
 pinMode(O1, OUTPUT);
 pinMode(O2, OUTPUT);
 pinMode(O3, OUTPUT);
 pinMode(O4, OUTPUT);
 pinMode(O5, OUTPUT);
//sei();//allow interrupts
}

void loop() {
  // put your main code here, to run repeatedly:
ScanStartMicros = micros();

//Future Use
 /* has a new command been set?
    if ( (gTriggerManual.Triggered() == true) || (gTriggerPLC.Triggered() == true) || (gTriggerUSB.Triggered() == true) )
    {
      pulseLenIn0_1Ms = gActivePulseTime->GetPulseTimeIn0_1ms();
      gRunMode.GetRunModeText3(strRunMode);
      gDiagLedMode = false;
      Serial.print(" MOD,");
      Serial.println(strRunMode);
      Serial.print("TPT,");
      Serial.println( pulseLenIn0_1Ms / 10.0);

      gPulseStateMachine.Triggered(pulseLenIn0_1Ms);
    }
  }
*/


 // check for serial activity
  gSerialCommands.readSerial();
  gScanRate = ElapsedTime(ScanStartMicros, micros());
  if (gScanRate < gScanRateMin) gScanRateMin = gScanRate;
  if (gScanRate > gScanRateMax) gScanRateMax = gScanRate;
}

/*char GetSerialCommandTxt(char cmd, char arg1, char arg2)
{

}
*/
void SetupSerialCommands(void)
{
  Serial.begin(kSerialBaudRate);
//The following statments create serial commands to communicate with the ardunioover USB:
//gSerialCommands.addCommand("<string to call cmd>", <function to execute>);
  gSerialCommands.addCommand("SOUT", SetOutput);        //Sets the specified output to the specified state.
  gSerialCommands.addCommand("TOUT", ToggleOutput);     //Toggles the specified output from the current output state.
  gSerialCommands.addCommand("POUT", PulseOutput);      //sets the output high for a specified amount of time.
  gSerialCommands.addCommand("PWM", PulseOutput);      //sets PWM output of a channel until told to turn off. updates to frequency and duty cyle should be avalable at runtime in the future. 
  gSerialCommands.addCommand("STAT", GetPinStates);     //gets the current state of each output PIN and displays it to the user.
  gSerialCommands.addCommand("DBUG", DebugPrint);       //Prints debug info that might be usefull
  gSerialCommands.addCommand("SRT?", CmdQueryScanRate); //Gets the scan rate of the last loop interation.
  gSerialCommands.setDefaultHandler(CmdUnknown);        //Default Handler used to send back a notification telling the sender that the command is Unknown (ex. UNK, <your-command>)
}

void SetOutput(void) //Called using 'SOUT'
// FINISHED: Completed by Ryan Baas Date: 20170206
/* COMMAND INFO & EXAMPLES:
 FORMAT: SOUT,<OUTPUT NUMBER>,<DESIRED STATE>    //Command is space sensitive.
     EX. SOUT,O1,1          //Sets OUTPUT 1 to the ON state.
     EX. SOUT,02,0          //Sets OUTPUT 2 to the OFF state.
     EX. SOUT,03,1          //Sets OUTPUT 3 to the ON state.
     EX. SOUT,04,1          //Sets OUTPUT 4 to the ON state.
     EX. SOUT,05,0          //Sets OUTPUT 5 to the OFF state.
     EX. SOUT,<OUTPUT NUMBER>,0    //Sets the specified OUTPUT to the OFF state.            */
{
  char *OutNum;                //creates a pointer to hold the first argument
  char *State;                  //creates a pointer to hold the second argument
  char *result;                 //creates a pointer for the ACK or NACK reponses
  //int state;
  //char *ErrorMsg;
  result = gNack;

  //Load command arguments into variables:
  OutNum = gSerialCommands.next();
  State = gSerialCommands.next();

  //Print command back to user
  Serial.print(OutNum);
  Serial.print(",");
  Serial.println(State);

    //Check to see if arguments were in fact acceptable:
    if (OutNum != NULL && (atoi(State) == 0 || atoi(State) == 1))
    {
      //SET Desired Output To Desired State:
      switch (atoi(OutNum))
      {
        case 1:
        digitalWrite(O1, atoi(State));
          break;
        case 2:
        digitalWrite(O2, atoi(State));
          break;
        case 3:
        digitalWrite(O3, atoi(State));
          break;
        case 4:
        digitalWrite(O4, atoi(State));
          break;
        case 5:
        digitalWrite(O5, atoi(State));
          break;
      }
      result = gAck;
    }
    //Print result:
    Serial.print(result);
    if (result == gAck)       //if ack then, print ack and display Current State of pin:
    {
      Serial.print(",");
      Serial.println(GetCurState(OutNum)); //This can be used as a confirmation that the pin got set HIGH.
    }
}

void ToggleOutput(void) //Called using 'TOUT'
// FINISHED: Completed by Ryan Baas Date: 20170206
/* COMMAND INFO & EXAMPLES:
 FORMAT: TOUT,<OUTPUT NUMBER>    //Command is space sensitive.
     EX. TOUT,O1         //Toggles OUTPUT 1 to the ON state.
     EX. TOUT,02         //Toggles OUTPUT 2 to the OFF state.
     EX. TOUT,03         //Toggles OUTPUT 3 to the ON state.
     EX. TOUT,04         //Toggles OUTPUT 4 to the ON state.
     EX. TOUT,05         //Toggles OUTPUT 5 to the OFF state.
     EX. TOUT,<OUTPUT NUMBER>    //Sets the specified OUTPUT to the opposite of the current state.            */
{
  char *OutNum;                 //creates a pointer to hold the first argument
  char *result;                 //creates a pointer for the ACK or NACK reponses
  bool CurState, NextState;
/*
 * Serial.println(bitRead(PORTD,3)); //Reads bit 3 of register PORTD which contains the current state (high/low) of pin 3.
 */
  result = gNack;

  //Load command arguments into variables: just a little change
  OutNum = gSerialCommands.next();


  CurState = GetCurState(OutNum);
/*  DEBUG CURRENT STATE
  Serial.println();
  Serial.print("CurState = ");
  Serial.println(CurState);     */
  //Print command back to user
  Serial.println(OutNum);


  //--------------------------
    //Find out what our next state is going to be.
    NextState = GetNextState(OutNum);
/*  DEBUG NEXT STATE
    Serial.print("NextState = ");
    Serial.println(NextState);
    */

    //Check to see if arguments were in fact acceptable:
    if (NextState != CurState )
    {
       //TOGGLE Desired Output:
       switch (atoi(OutNum))
      {
        case 1:
        digitalWrite(O1, GetNextState(OutNum));
          break;
        case 2:
        digitalWrite(O2, GetNextState(OutNum));
          break;
        case 3:
        digitalWrite(O3, GetNextState(OutNum));
          break;
        case 4:
        digitalWrite(O4, GetNextState(OutNum));
          break;
        case 5:
        digitalWrite(O5, GetNextState(OutNum));
          break;
      }
      result = gAck;
    }
    //Print result:
    Serial.print(result);
    if (result == gAck)       //if ack then, print ack and display Current State of pin:
    {
      Serial.print(",");
      Serial.println(GetCurState(OutNum)); //This can be used as a confirmation that the pin got set HIGH.
    }

}

void PulseOutput(void) //Called using 'POUT'
// NEEDS WORK!! Might be finished..:
/* COMMAND INFO & EXAMPLES:
 FORMAT: POUT,<OUTPUT NUMBER>,<PULSE DURATION>    //Command is space sensitive.
     EX. POUT,O1,500      //Toggles OUTPUT 1 to the ON state for 500ms then goes LOW.
     EX. POUT,02         //Toggles OUTPUT 2 to the OFF state.
     EX. POUT,03         //Toggles OUTPUT 3 to the ON state.
     EX. POUT,04         //Toggles OUTPUT 4 to the ON state.
     EX. POUT,05         //Toggles OUTPUT 5 to the OFF state.
     EX. POUT,<OUTPUT NUMBER>,<PULSE DURATION>    //Sets the specified OUTPUT to the opposite of the current state.            */
{
  char *OutNum;                 //creates a pointer to hold the first argument
  char *PulseTime;              //creates a pointer to hold the second argument
  char *result;                 //creates a pointer for the ACK or NACK reponses

  result = gNack;

   //Load command arguments into variables:
    OutNum = gSerialCommands.next();
    PulseTime = gSerialCommands.next();

    //Print command back to user
    Serial.print(OutNum);
    Serial.print(",");
    Serial.println(PulseTime);

    //check to see if the arguments are acceptable
    /*for (int i; i <= 4; i++)
    {
      if (OutNum == gOutAliases[i])
      {
        result = gAck;
      }
      //Serial.println(gOutAliases[i]);
    }
    */

    if (OutNum != NULL && PulseTime != NULL && PulseTime < 10000)
    {
      switch (atoi(OutNum))
      {
        case 1:
          digitalWrite(O1, HIGH);
          delay(long(PulseTime));
          digitalWrite(O1, LOW);
          break;
        case 2:
          digitalWrite(O2, HIGH);
          delay(long(PulseTime));
          digitalWrite(O2, LOW);
          break;
        case 3:
          digitalWrite(O3, HIGH);
          delay(long(PulseTime));
          digitalWrite(O3, LOW);
          break;
        case 4:
          digitalWrite(O4, HIGH);
          delay(long(PulseTime));
          digitalWrite(O4, LOW);
          break;
        case 5:
          digitalWrite(O5, HIGH);
          delay(long(PulseTime));
          digitalWrite(O5, LOW);
          break;
      }
      result = gAck;
    }
    //Print result:
    Serial.print(result);
   /* if (result == gAck)       //if ack then, print ack and display Current State of pin:
    {
      Serial.print(",");
      Serial.println(PULSETIME MESURMENT); //This can be used as a confirmation that the pin got set HIGH.
    }   */
}
void SetPWM(void)
{
 // NEEDS WORK!!
/* COMMAND INFO & EXAMPLES:
 FORMAT: PWM,<PIN>,<DESIRED-ANGLE>    //<FREQUENCY><DUTY-CYCLE>    //Command is space sensitive.
     EX. PWM,11,22,50      //Sets pin 11 to output PWM with 22_Hz and 50% Duty Cycle
     */
//INITIALIZE PARAMETER POINTERS
char *result, *iMtrNumb, *iFreq, *iDuty;   //, *iFreq, *iDutyCycle, *result;


//SAVE INPUT PARAMETERS TO VARIABLES
iOutNumb = cm.next();
iFreq = cm.next();
iDuty = cm.next();
result = gNack;

if(iOutNumb != NULL && iFreq != NULL && iDuty != NULL) //if we have a argument(Pin Number),
  {
    //Print Out Command:
    Serial.print(iOutNumb);
    Serial.print(",");
    Serial.println(iFreq);
    Serial.print(",");
    Serial.println(iDuty);
    //UPDATE and Print Out Response:
    result = gAck;
    Serial.println(result);

//Move MTR To Positon
// This should be re done into a class
//Serial.print("Previous position = ");
//Serial.println(motor1.GetCurDegs());
//motor1.PWM(atof(iFreq), atof(iDuty));

    }
}

void DebugPrint(void) //FUNCTION FOR SERIAL DEBUGING
{
  //Your Debugging Statments Here:
  //Serial.println("Put your DEBUGGING code in the function 'DebugPrint' and it will be displayed here.");

  Serial.println("Freq = " + String(motor1.GetFreq()));

      Serial.println("Period = " + String(motor1.GetPeriod()));
        Serial.println("Duty = " + String(motor1.GetDuty()));
          Serial.println("Dutymicros = " + String(motor1.GetDutymicros()));
            Serial.println("StepsPerRev = " + String(motor1.GetStepsPerRev()));
              Serial.println("CurStepCount = " + String(motor1.GetCurStepCount()));

  /*for (int i = 7;i<=8;i++)
  {
  Serial.println();
  Serial.println(GetCurState(i2str(i,gbuf)));
} */
}

void GetPinStates(void) //gets the current state of each output PIN and displays it to the user.
{                       //CALLED BY 'STAT'   :     Completed by Ryan Baas Date: 20170206

  char *OutNum, *result;
  int j;
  //char* PinStatestxt[4];
  bool PinState;

  ////Load command arguments into variables & initalize result:
  OutNum = gSerialCommands.next();
  result = gNack;

  if(OutNum != NULL) //if we have a argument, send back only the specified pin state:
  {
    //Print Out Command:
    Serial.println(OutNum);
    //UPDATE and Print Out Response:
    result = gAck;
    Serial.print(result);
    Serial.print(",");
    Serial.println(GetCurState(OutNum));
  }
  else
  {
    //Print result, NL, Table format, then data.
    result = gAck;
    Serial.println(result);
    Serial.println();                       //NL
    Serial.println(" Output | State ");     //Table Headers
      Serial.println("--------|-------");     //Dividers
    //Get each state
    for (int i; i <= 4; i++)
    {
      j = i+1;
      Serial.print("   ");                  //BlankSpace
      Serial.print(gOutAliases[i]);         //Print output number
           Serial.print("   |   ");              //Spaces (Future)
      Serial.print(GetCurState(i2str(j,gbuf)));   //Print current state
                 Serial.println("   ");                  //Spcaces (Future)
    }
  }
}

//Serial.println(bitRead(PORTD,3)); //Reads bit 3 of register PORTD which contains the current state (high/low) of pin 3.

bool GetCurState(char *iOutNum) //gets the current state of the specified output PIN.
{
  /* What Made This Possible...
   * Serial.println(bitRead(PORTD,3)); //Reads bit 3 of register PORTD which contains the current state (high/low) of pin 3.
   */
  int pin;
  bool PinState;
  switch (atoi(iOutNum))
      {
        case 1:
        pin= O1;
        PinState = (0!=(*portOutputRegister( digitalPinToPort(pin) ) & digitalPinToBitMask(pin)));
          break;
        case 2:
        pin= O2;
        PinState = (0!=(*portOutputRegister( digitalPinToPort(pin) ) & digitalPinToBitMask(pin)));
          break;
        case 3:
        pin= O3;
       PinState = (0!=(*portOutputRegister( digitalPinToPort(pin) ) & digitalPinToBitMask(pin)));
          break;
        case 4:
        pin= O4;
        PinState = (0!=(*portOutputRegister( digitalPinToPort(pin) ) & digitalPinToBitMask(pin)));
          break;
        case 5:
        pin= O5;
        PinState = (0!=(*portOutputRegister( digitalPinToPort(pin) ) & digitalPinToBitMask(pin)));
          break;
      }
  return PinState;
}



bool GetNextState(char *iOutNum) //gets the NEXT state of the specified pin number.
                                //Will return TRUE or FALSE or HIGH or LOW  :   Completed by Ryan Baas Date: 20170206
{
  bool PinState, NextPinState;

  PinState = GetCurState(iOutNum);
  NextPinState = !PinState;
  return NextPinState;
}



void CmdUnknown(const char *iCommand) //Called on anything sent that is not defined as a command (ex. 'TPGTRIG')
{
  Serial.print("UNK,");
  Serial.println(iCommand);
}

/* Calculates the time elapsed between the start time and the current time
 Assumes the start should always be before the current and then compensates
 if the milli or micro clock has rolled over    */
unsigned long ElapsedTime(unsigned long iStartTime, unsigned long iCurTime)
{
  unsigned long elapsedTime;

  if (iCurTime >= iStartTime)
  {
    elapsedTime = iCurTime - iStartTime;
  }
  else // clock rolled over
  {
    elapsedTime =  (unsigned long)0xFFFFFFFF - iStartTime + iCurTime;
  }
  return elapsedTime;
}

void DebugPrint(void) //FUNCTION FOR SERIAL DEBUGING
{
  //Your Debugging Statments Here:
  Serial.println("Put your DEBUGGING code in the function 'DebugPrint' and it will be displayed here.");
// Old Debugging
/*  int pin = O1;
  bool PinState = (0!=(*portOutputRegister( digitalPinToPort(O1) ) & digitalPinToBitMask(O1)));
  Serial.println("");
  Serial.print("OUT");
  Serial.print("1");
  Serial.print(" Current State: ");
  Serial.println(PinState);
  */
}

void CmdQueryScanRate(void) //Called using 'SRT?'
/* COMMAND INFO & EXAMPLES:
 FORMAT: SRT?               //Command has no parameters.
     EX. SRT?               //Returns the scan rate of the previous microprocessor loop.  */
{
  Serial.print(gScanRate);
  Serial.print(",");
  Serial.print(gScanRateMin);
  Serial.print(",");
  Serial.println(gScanRateMax);
}
char *i2str(int i, char *buf) //FUNNCTION TAKEN FROM ONLINE: INT TO STRING
{
  byte l=0;
  if(i<0) buf[l++]='-';
  boolean leadingZ=true;
  for(int div=10000, mod=0; div>0; div/=10){
    mod=i%div;
    i/=div;
    if(!leadingZ || i!=0){
       leadingZ=false;
       buf[l++]=i+'0';
    }
    i=mod;
  }
  buf[l]=0;
  return buf;
}
