#ifndef SigPWM_H
#define SigPWM_H

#include <Arduino.h>
//#include <SdFat.h>

#include <EEPROMex.h>
class SigPWM
{
public:
SigPWM(const int iOutPin, double iFreq, int iDuty, const int iDisablePin);
~SigPWM(void);

void Save2EEPROM(void);
void ReadEEPROM(void);
int Degree2Steps(double iDegrees);
void MoveFor(double iTime);
void SetFreq(int iFreq, int iPeriod_ms); //choose one
int GetFreq(void);
double GetPeriod(void);
void SetDuty(int iPercent0_100);
double GetDuty(void);
double GetDutymicros(void);

void SetStepsPerUnit(int iStepsPerRev);
double GetStepsPerUnit(void);
unsigned long GetCurStepCount(void);
void GetCurUnit(void);
void SetTimerValue(double iSeconds);
void SetSaveState(bool i);

//bool GreaterThan100();
protected:
int mOutPin, mDisablePin, mStepsPerUnit;
bool mSaveState, mPWM_ON;
double mFreq;
double mDuty;
double mDutymicros;
double mPeriod_us;
unsigned long mStepCount; //Gloabl Step count
};



#endif
