#include <SigPWM.h>

SigPWM::SigPWM(const int iOutPin, double iFreq, double iDuty, const int iDisablePin)
{
  mOutPin = iOutPin;
  mDisablePin = iDisablePin;
  mFreq = iFreq;
  mPeriod_us = 1.0e6/iFreq;
  mDuty = iDuty;
  mDutymicros = 0.01*iDuty*mPeriod_us;
}

SigPWM::~SigPWM(void){
;;
}

int SigPWM::Degree2Steps(double iDegrees){
//Converts input Degrees into Steps.
//This Depends on the Steps Per Rev of the motor. this example will use a 2000 Steps/Rev.
// StepsPerDeg = {# of Steps to go 360 Degrees} / {Degrees Per Rev (I would hope this is 360)}
mStepsPerDeg = mStepsPerRev/360;
//mDegPerStep = 1/mStepsPerDeg;
return iDegrees*mStepsPerDeg;

}

void SigPWM::Move(double iUnit) // goes to the specified degrees between 0 and 360
{
  mPWM_ON = true;

  int iSteps = Unit2Steps(iUnit);
 
  for (int i = 0; i <= iSteps; i += 1)  // goes from 0 degrees to 180 degrees
    {
    digitalWrite(mOutPin, HIGH);
    delayMicroseconds(mDutymicros); // if = 1 ... then, Approximately 1% duty cycle @ 10KHz
    digitalWrite(mOutPin, LOW);
    delayMicroseconds(mPeriod_us - mDutymicros); // if = 100-1
    mStepCount++;

    }


    Serial.print("Step Cout of last run:");
    Serial.println(mStepCount);
    Serial.println("DONE");

}

void SigPWM::MoveTo(double iDegrees) // goes to the specified degrees between 0 and 360
{
  double OldPositionDeg = mCurPositionDeg;

// compare current location to requested location.
double DiffDeg;
if (mCurPositionDeg > iDegrees){
  double Diff2Zero = 360 - mCurPosition;
  DiffDeg = Diff2Zero + iDegrees;
}
elseif (mCurPositionDeg < iDegrees){
  DiffDeg = iDegrees - mCurPosition;
}
elseif (mCurPositionDeg = iDegrees){
  DiffDeg = 0;
}


  if (DiffDeg != 0){

    int iSteps = Degree2Steps(DiffDeg);
    #ifdef DEBUG
      Serial.print("DBUG: iDegrees = ");
      Serial.println(iDegrees);
    #endif
  for (int i = 0; i <= iSteps; i += 1)  // goes from 0 degrees to iDegrees
    {
    digitalWrite(mOutPin, HIGH);
    delayMicroseconds(int(mDutymicros)); // if = 1 ... then, Approximately 1% duty cycle @ 10KHz
    digitalWrite(mOutPin, LOW);
    delayMicroseconds(int(mPeriod_us - mDutymicros)); // if = 100-1
    mStepCount++; //should add if direction changes then we subtract.
    mAbsPositionStep++;
    mCurPositionDeg += (1/mStepsPerDeg);
    mAbsPositionDeg += (1/mStepsPerDeg);

    if (mCurPositionDegree >= 360){
      // should prolly account for the chance mCurPositionDeg intterates to 360.2 deg and set the new value to 0.2 deg
      mCurPositionDeg = 0;
    }
    }
    //#ifdef DEBUG

    Serial.print("Old Position:");
    Serial.println(OldPositionDeg);
    Serial.println();

    Serial.print("New Position:");
    Serial.println(mCurPositionDeg);
    Serial.println("DONE");
    //#endif
  }
  Serial.pintln("The motor is already at the specified location.");
}

void SigPWM::SetFreq(int iFreq, int iPeriod_us){

  if (iFreq == -1){
    mFreq = 1/iPeriod_us;
    #ifdef DEBUG
      Serial.println("mFreq = %i set from Period.", mFreq);
    #endif
  }
  else{
    mFreq = iFreq;
    mPeriod_us = 1/iFreq;
    SetDuty(mDuty); //Needed to update mDutymicros
  }
}

int SigPWM::GetFreq(void){
  return mFreq;
}

double SigPWM::GetPeriod(void){
  return mPeriod_us;
}

void SigPWM::SetDuty(int iPercent0_100){

  mDuty = iPercent0_100;
  mDutymicros = mDuty*mPeriod_us;
}

double SigPWM::GetDuty(void){
  return mDuty;
}

double SigPWM::GetDutymicros(void){
  return  mDutymicros;
}

void SigPWM::SetStepsPerUnit(int iStepsPerRev){
    mStepsPerRev = iStepsPerRev;
}

double SigPWM::GetStepsPerUnit(void){
    return mStepsPerRev;
}

unsigned long SigPWM::GetCurStepCount(void){
    return mStepCount; // in steps
}

double SigPWM::GetCurUnit(void){
    return mCurPositionDeg;
}

void SigPWM::SetHome(void){
// Resets the Position of to 0 and makes the current location the home. cannot be called while moving.
mCurPositionDeg = 0;
mStepCount = 0;
}

void SigPWM::SetSaveState(bool i){
mSaveState = i;
}

void SigPWM::Save2EEPROM(void){
//Save all member variables to EEPROM ino order to save state incase a reset occurs
int address=0;
//address0 = getAddress(int noOfBytes); //Geets the first open address in EEPROM
//calculated or values that update
EEPROM.writeDouble(address, mCurPositionDeg); //Write EEPROM Adress 0-31
address += sizeof(mCurPositionDeg); //Sets adress variable to 32
EEPROM.updateLong(address, mStepCount); //Write EEPROM adress 32-63
address += sizeof(mStepCount); //Sets address to 64
EEPROM.updateLong(address, mAbsPositionDeg);
address += sizeof(mAbsPositionDeg);
EEPROM.updateLong(address, mAbsPositionStep);
address += sizeof(mAbsPositionStep);

//Required Values to keep constant.
EEPROM.update(address, mOutPin);
address += sizeof(mOutPin);
EEPROM.update(address, mDisablePin);
address += sizeof(mDisablePin);
EEPROM.update(address, mDirecPin);
address += sizeof(mDirecPin);
EEPROM.updateDouble(address, mFreq);
address += sizeof(mFreq);
EEPROM.updateDouble(address, mDuty);
address += sizeof(mDuty);
EEPROM.updateDouble(address, mStepsPerRev);
address += sizeof(mStepsPerRev);

}

void SigPWM::ReadEEPROM(void){
// Read all back into the stack.

//EEPROM.readDouble();



;; //later will prolly need to make this go in main cpp

}
